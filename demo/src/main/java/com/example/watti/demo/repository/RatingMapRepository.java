package com.example.watti.demo.repository;


import com.example.watti.demo.entity.Rating;
import com.example.watti.demo.entity.RatingMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingMapRepository extends JpaRepository<RatingMap,Integer> {
}
