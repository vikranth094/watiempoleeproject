package com.example.watti.demo.dto;

import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;

@Data
public class RatingDto {
    Integer employeeId;
    Integer ratingId;
}
