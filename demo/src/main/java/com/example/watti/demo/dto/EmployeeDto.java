package com.example.watti.demo.dto;

import lombok.Data;

@Data
public class EmployeeDto {
    private String firstName;
    private String secondName;
    private  String salary;
}
