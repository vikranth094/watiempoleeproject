package com.example.watti.demo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity(name = "emp_rating_map")
public class RatingMap {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private  int empId;
    private  int ratingId;

}
