package com.example.watti.demo.service;

import com.example.watti.demo.dto.EmployeeDto;
import com.example.watti.demo.dto.RatingDto;
import com.example.watti.demo.entity.Employee;
import com.example.watti.demo.entity.Rating;
import com.example.watti.demo.entity.RatingMap;
import com.example.watti.demo.repository.EmployeeRepository;
import com.example.watti.demo.repository.RatingMapRepository;
import com.example.watti.demo.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    RatingMapRepository ratingMapRepository;
    public  Employee createEmployee(EmployeeDto employeeDto)
    {
        Employee employee = new Employee();
        employee.setFirstName(employeeDto.getFirstName());
        employee.setSecondName(employeeDto.getSecondName());
        employee.setSalary(employeeDto.getSalary());
        return employeeRepository.save(employee);


    }
    public  Employee getEmploye(Integer id)
    {

        return employeeRepository.getOne(id);


    }

    public  String applyRating(RatingDto ratingDto)
    {
        Employee employee = employeeRepository.getOne(ratingDto.getEmployeeId());
        Rating rating = ratingRepository.getOne(ratingDto.getRatingId());
        Double salary = Double.parseDouble(employee.getSalary());
        double newSalary = salary + salary * rating.getHikePercentage() /100;
        employee.setSalary(String.valueOf(newSalary));
        RatingMap ratingMap = new RatingMap();
        ratingMap.setRatingId(rating.getId());
        ratingMap.setEmpId(employee.getId());
        ratingMapRepository.save(ratingMap);
        employeeRepository.save(employee);

        return  "Revised salary :"+salary;
    }
}
