package com.example.watti.demo.controller;

import com.example.watti.demo.dto.EmployeeDto;
import com.example.watti.demo.dto.RatingDto;
import com.example.watti.demo.entity.Employee;
import com.example.watti.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @PostMapping("/employee")
    public ResponseEntity<Employee> createEmployee(@RequestBody EmployeeDto employee)
    {
        return new ResponseEntity<>(
                employeeService.createEmployee(employee),
                HttpStatus.OK);
    }
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable Integer id)
    {
        return new ResponseEntity<>(
                employeeService.getEmploye(id),
                HttpStatus.OK);
    }

    @PostMapping("/employee/applyRating")
    public ResponseEntity<String> applyRating(@RequestBody RatingDto ratingDto)
    {
        return new ResponseEntity<String>(
                employeeService.applyRating(ratingDto),
                HttpStatus.OK);
    }

}
