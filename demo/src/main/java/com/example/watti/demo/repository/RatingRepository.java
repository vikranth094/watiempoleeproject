package com.example.watti.demo.repository;


import com.example.watti.demo.entity.Employee;
import com.example.watti.demo.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends JpaRepository<Rating,Integer> {
}
